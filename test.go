package main

import (
	"fmt"
	"time"
)

type Info struct {
	InfoProyek string
	LastUpdate time.Time
}

func (fal *Info) UpdateLog(log string) string {
	fal.InfoProyek = log
	fal.LastUpdate = time.Now()
	return fmt.Sprintf("%v\n\nLastTest: %v", fal.InfoProyek, fal.LastUpdate)
}

func main() {
	x64 := &Info{}
	fmt.Println(x64.UpdateLog("Project ini dibuat untuk belajar bagaimana cara menggunakan gitlab"))
}
